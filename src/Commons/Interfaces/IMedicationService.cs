﻿using Commons.DTOs;
using System.Collections.Generic;

namespace Commons.Interfaces
{
    public interface IMedicationService
    {
        Medication AddMedication(MedicationAddDTO medicationToAdd);

        IEnumerable<Medication> GetAllMedication();

        void DeleteMedication(string id);
    }
}
