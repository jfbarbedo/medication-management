using Commons.DTOs;
using Commons.Exceptions;
using NUnit.Framework;
using System;

namespace Validators.Tests
{
    [TestFixture()]
    public class MedicationValidatorTests
    {
        [Test]
        public void ValidateMedicationId_NullId_ThrowsNullOrEmptyMedicationIdException()
        {
            string id = null;

            var validator = new MedicationValidator();

            Assert.Throws<NullOrEmptyMedicationIdException>(() => validator.ValidateMedicationId(id));
        }

        [Test]
        public void ValidateMedicationId_EmptyStringId_ThrowsNullOrEmptyMedicationIdException()
        {
            string id = "";

            var validator = new MedicationValidator();

            Assert.Throws<NullOrEmptyMedicationIdException>(() => validator.ValidateMedicationId(id));
        }

        [Test]
        public void ValidateMedicationId_NormalId_ThrowsNullOrEmptyMedicationIdException()
        {
            string id = "f96f8380-aca7-4137-a3a3-e419c394f6c7";

            var validator = new MedicationValidator();

            Assert.DoesNotThrow(() => validator.ValidateMedicationId(id));
        }

        [Test]
        public void ValidateMedicationToAdd_NormalMedicationToAdd_ThrowsNullOrEmptyMedicationIdException()
        {
            var medicationToAdd = new MedicationAddDTO
            {
                Name = "Vicodin",
                Quantity = 9514,
                CreationDate = DateTime.Parse("1978-01-01T00:00:00")
            };

            var validator = new MedicationValidator();

            Assert.DoesNotThrow(() => validator.ValidateMedicationToAdd(medicationToAdd));
        }

        [Test]
        public void ValidateMedicationId_MedicationToAddWithNullName_ThrowsNullOrEmptyMedicationNameException()
        {
            var medicationToAdd = new MedicationAddDTO
            {
                Name = null,
                Quantity = 9514,
                CreationDate = DateTime.Parse("1978-01-01T00:00:00")
            };

            var validator = new MedicationValidator();

            Assert.Throws<NullOrEmptyMedicationNameException>(() => validator.ValidateMedicationToAdd(medicationToAdd));
        }

        [Test]
        public void ValidateMedicationId_MedicationToAddWithZeroQuantity_ThrowsLowerOrEqualToZeroMedicationQuantityException()
        {
            var medicationToAdd = new MedicationAddDTO
            {
                Name = "Vicodin",
                Quantity = 0,
                CreationDate = DateTime.Parse("1978-01-01T00:00:00")
            };

            var validator = new MedicationValidator();

            Assert.Throws<LowerOrEqualToZeroMedicationQuantityException>(() => validator.ValidateMedicationToAdd(medicationToAdd));
        }

        [Test]
        public void ValidateMedicationId_MedicationToAddWithNegativeQuantity_ThrowsLowerOrEqualToZeroMedicationQuantityException()
        {
            var medicationToAdd = new MedicationAddDTO
            {
                Name = "Vicodin",
                Quantity = -1,
                CreationDate = DateTime.Parse("1978-01-01T00:00:00")
            };

            var validator = new MedicationValidator();

            Assert.Throws<LowerOrEqualToZeroMedicationQuantityException>(() => validator.ValidateMedicationToAdd(medicationToAdd));
        }
    }
}