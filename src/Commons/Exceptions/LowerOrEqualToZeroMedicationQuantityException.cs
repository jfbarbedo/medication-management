﻿using System;

namespace Commons.Exceptions
{
    public class LowerOrEqualToZeroMedicationQuantityException : Exception
    {
        public LowerOrEqualToZeroMedicationQuantityException(string message, Exception innerException) : base(message, innerException)
        {
        }

        public LowerOrEqualToZeroMedicationQuantityException(string message) : base(message)
        {
        }
    }
}
