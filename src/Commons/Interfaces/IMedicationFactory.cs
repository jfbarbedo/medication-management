﻿using Commons.DTOs;

namespace Commons.Interfaces
{
    public interface IMedicationFactory
    {
        Medication CreateMedication(string id, MedicationAddDTO medication);

        Medication CreateMedication(MedicationAddDTO medicationToAdd);
    }
}
