﻿using Commons.DTOs;
using System.Collections.Generic;

namespace Gateways.JsonFile.Interfaces
{
    public interface IJsonLoader
    {
        IEnumerable<Medication> LoadJsonFile();
    }
}
