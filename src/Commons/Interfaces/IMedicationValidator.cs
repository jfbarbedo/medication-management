﻿using Commons.DTOs;

namespace Commons.Interfaces
{
    public interface IMedicationValidator
    {
        void ValidateMedicationId(string id);

        void ValidateMedicationToAdd(MedicationAddDTO medicationToAdd);
    }
}
