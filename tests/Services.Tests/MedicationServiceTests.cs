using Commons.DTOs;
using Commons.Interfaces;
using Moq;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Services.Tests
{
    [TestFixture()]
    public class MedicationServiceTests
    {
        [Test]
        public void AddMedication_NormalMedicationToAdd_ReturnsWithMedicationCreated()
        {
            var medicationToAdd = new MedicationAddDTO
            {
                Name = "Vicodin",
                Quantity = 9514,
                CreationDate = DateTime.Parse("1978-01-01T00:00:00")
            };

            var medicationCreated = new Medication
            {
                Id = "f96f8380-aca7-4137-a3a3-e419c394f6c7",
                Name = "Vicodin",
                Quantity = 9514,
                CreationDate = DateTime.Parse("1978-01-01T00:00:00")
            };

            var repository = new Mock<IMedicationRepository>();
            var validator = new Mock<IMedicationValidator>();
            var factory = new Mock<IMedicationFactory>();

            validator.Setup(v => v.ValidateMedicationToAdd(It.IsAny<MedicationAddDTO>()));

            repository.Setup(r => r.Add(It.IsAny<MedicationAddDTO>()));

            factory.Setup(f => f.CreateMedication(It.IsAny<string>(), It.IsAny<MedicationAddDTO>()))
                .Returns(medicationCreated);

            var service = new MedicationService(repository.Object, validator.Object, factory.Object);

            var result = service.AddMedication(medicationToAdd);

            validator.Verify(v => v.ValidateMedicationToAdd(It.IsAny<MedicationAddDTO>()), Times.Once);
            repository.Verify(r => r.Add(It.IsAny<MedicationAddDTO>()), Times.Once);
            factory.Verify(f => f.CreateMedication(It.IsAny<string>(), It.IsAny<MedicationAddDTO>()), Times.Once);

            Assert.IsNotNull(result);
            Assert.AreEqual("f96f8380-aca7-4137-a3a3-e419c394f6c7", result.Id);
        }

        [Test]
        public void AddMedication_ErrorWhileValidating_ThrowsException()
        {
            var medicationToAdd = new MedicationAddDTO
            {
                Name = "Vicodin",
                Quantity = 9514,
                CreationDate = DateTime.Parse("1978-01-01T00:00:00")
            };

            var exceptionMessage = "exception message";

            var repository = new Mock<IMedicationRepository>();
            var validator = new Mock<IMedicationValidator>();
            var factory = new Mock<IMedicationFactory>();

            validator.Setup(v => v.ValidateMedicationToAdd(It.IsAny<MedicationAddDTO>()))
                .Throws(new Exception(exceptionMessage));

            var service = new MedicationService(repository.Object, validator.Object, factory.Object);

            Assert.Throws<Exception>(() => service.AddMedication(medicationToAdd));

            validator.Verify(v => v.ValidateMedicationToAdd(It.IsAny<MedicationAddDTO>()), Times.Once);
            repository.Verify(r => r.Add(It.IsAny<MedicationAddDTO>()), Times.Never);
            factory.Verify(f => f.CreateMedication(It.IsAny<string>(), It.IsAny<MedicationAddDTO>()), Times.Never);
        }

        [Test]
        public void AddMedication_ErrorWhileAddingDataOnRepo_ThrowsException()
        {
            var medicationToAdd = new MedicationAddDTO
            {
                Name = "Vicodin",
                Quantity = 9514,
                CreationDate = DateTime.Parse("1978-01-01T00:00:00")
            };

            var exceptionMessage = "exception message";

            var repository = new Mock<IMedicationRepository>();
            var validator = new Mock<IMedicationValidator>();
            var factory = new Mock<IMedicationFactory>();

            validator.Setup(v => v.ValidateMedicationToAdd(It.IsAny<MedicationAddDTO>()));
            repository.Setup(r => r.Add(It.IsAny<MedicationAddDTO>()))
                .Throws(new Exception(exceptionMessage));

            var service = new MedicationService(repository.Object, validator.Object, factory.Object);

            Assert.Throws<Exception>(() => service.AddMedication(medicationToAdd));

            validator.Verify(v => v.ValidateMedicationToAdd(It.IsAny<MedicationAddDTO>()), Times.Once);
            repository.Verify(r => r.Add(It.IsAny<MedicationAddDTO>()), Times.Once);
            factory.Verify(f => f.CreateMedication(It.IsAny<string>(), It.IsAny<MedicationAddDTO>()), Times.Never);
        }

        [Test]
        public void AddMedication_ErrorWhileCreatingMedicationCreatedObject_ThrowsException()
        {
            var medicationToAdd = new MedicationAddDTO
            {
                Name = "Vicodin",
                Quantity = 9514,
                CreationDate = DateTime.Parse("1978-01-01T00:00:00")
            };

            var exceptionMessage = "exception message";

            var repository = new Mock<IMedicationRepository>();
            var validator = new Mock<IMedicationValidator>();
            var factory = new Mock<IMedicationFactory>();

            validator.Setup(v => v.ValidateMedicationToAdd(It.IsAny<MedicationAddDTO>()));
            repository.Setup(r => r.Add(It.IsAny<MedicationAddDTO>()));
            factory.Setup(f => f.CreateMedication(It.IsAny<string>(), It.IsAny<MedicationAddDTO>()))
                .Throws(new Exception(exceptionMessage));

            var service = new MedicationService(repository.Object, validator.Object, factory.Object);

            Assert.Throws<Exception>(() => service.AddMedication(medicationToAdd));

            validator.Verify(v => v.ValidateMedicationToAdd(It.IsAny<MedicationAddDTO>()), Times.Once);
            repository.Verify(r => r.Add(It.IsAny<MedicationAddDTO>()), Times.Once);
            factory.Verify(f => f.CreateMedication(It.IsAny<string>(), It.IsAny<MedicationAddDTO>()), Times.Once);
        }

        [Test]
        public void GetAllMedication_ErrorWhileGettingDataFromRepo_ThrowsException()
        {
            var exceptionMessage = "exception message";

            var repository = new Mock<IMedicationRepository>();
            var validator = new Mock<IMedicationValidator>();
            var factory = new Mock<IMedicationFactory>();

            repository.Setup(r => r.GetAll())
                .Throws(new Exception(exceptionMessage));

            var service = new MedicationService(repository.Object, validator.Object, factory.Object);

            Assert.Throws<Exception>(() => service.GetAllMedication());

            repository.Verify(r => r.GetAll(), Times.Once);
        }

        [Test]
        public void GetAllMedication_NormalRequest_ThrowsException()
        {
            var medications = new List<Medication>
            {
                new Medication
                {
                    Id = "f96f8380-aca7-4137-a3a3-e419c394f6c7",
                    Name = "Vicodin",
                    Quantity = 9514,
                    CreationDate = DateTime.Parse("1978-01-01T00:00:00")
                },
                new Medication
                {
                    Id = "3dec7dc0-3650-4b07-864f-3dfbef71b83f",
                    Name = "Albuterol",
                    Quantity = 2589,
                    CreationDate = DateTime.Parse("1972-10-21T00:00:00")
                }
            };

            var repository = new Mock<IMedicationRepository>();
            var validator = new Mock<IMedicationValidator>();
            var factory = new Mock<IMedicationFactory>();

            repository.Setup(r => r.GetAll())
                .Returns(medications);

            var service = new MedicationService(repository.Object, validator.Object, factory.Object);

            var result = service.GetAllMedication();

            repository.Verify(r => r.GetAll(), Times.Once);
            Assert.IsNotNull(result);
            Assert.AreEqual(2, result.Count());
        }

        [Test]
        public void DeleteMedication_NormalStringId_ReturnsWithSucess()
        {
            var id = "f96f8380-aca7-4137-a3a3-e419c394f6c7";

            var repository = new Mock<IMedicationRepository>();
            var validator = new Mock<IMedicationValidator>();
            var factory = new Mock<IMedicationFactory>();

            validator.Setup(v => v.ValidateMedicationId(It.IsAny<string>()));

            repository.Setup(r => r.Delete(It.IsAny<string>()));

            var service = new MedicationService(repository.Object, validator.Object, factory.Object);

            service.DeleteMedication(id);

            validator.Verify(v => v.ValidateMedicationId(It.IsAny<string>()), Times.Once);
            repository.Verify(r => r.Delete(It.IsAny<string>()), Times.Once);
        }

        [Test]
        public void DeleteMedication_ErrorWhileValidating_ThrowsException()
        {
            var id = "";
            var exceptionMessage = "exception message";

            var repository = new Mock<IMedicationRepository>();
            var validator = new Mock<IMedicationValidator>();
            var factory = new Mock<IMedicationFactory>();

            validator.Setup(v => v.ValidateMedicationId(It.IsAny<string>()))
                .Throws(new Exception(exceptionMessage));

            var service = new MedicationService(repository.Object, validator.Object, factory.Object);

            Assert.Throws<Exception>(() => service.DeleteMedication(id));

            validator.Verify(v => v.ValidateMedicationId(It.IsAny<string>()), Times.Once);
            repository.Verify(r => r.Delete(It.IsAny<string>()), Times.Never);
        }

        [Test]
        public void DeleteMedication_ErrorWhileDeletingDataFromRepo_ThrowsException()
        {
            var id = "f96f8380-aca7-4137-a3a3-e419c394f6c7";
            var exceptionMessage = "exception message";

            var repository = new Mock<IMedicationRepository>();
            var validator = new Mock<IMedicationValidator>();
            var factory = new Mock<IMedicationFactory>();

            validator.Setup(v => v.ValidateMedicationId(It.IsAny<string>()));

            repository.Setup(r => r.Delete(It.IsAny<string>()))
                .Throws(new Exception(exceptionMessage));

            var service = new MedicationService(repository.Object, validator.Object, factory.Object);

            Assert.Throws<Exception>(() => service.DeleteMedication(id));

            validator.Verify(v => v.ValidateMedicationId(It.IsAny<string>()), Times.Once);
            repository.Verify(r => r.Delete(It.IsAny<string>()), Times.Once);
        }
    }
}