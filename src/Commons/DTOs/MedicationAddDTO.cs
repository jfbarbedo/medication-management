﻿using System;

namespace Commons.DTOs
{
    public class MedicationAddDTO
    {
        public string Name { get; set; }
        public int Quantity { get; set; }
        public DateTime CreationDate { get; set; }
    }
}
