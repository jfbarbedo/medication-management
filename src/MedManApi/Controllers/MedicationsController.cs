﻿using Commons.DTOs;
using Commons.Exceptions;
using Commons.Interfaces;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;

namespace MedManApi.Controllers
{
    [ApiController]
    [Route("medications")]
    public class MedicationsController : ControllerBase
    {
        private readonly IMedicationService _medicationService;
        private readonly ILogger<MedicationsController> _logger;

        public MedicationsController(ILogger<MedicationsController> logger, IMedicationService medicationService)
        {
            _medicationService = medicationService;
            _logger = logger;
        }

        [HttpGet]
        public IActionResult GetMedications()
        {
            try
            {
                return Ok(_medicationService.GetAllMedication());
            }
            catch (Exception ex)
            {
                return HandleGenericunHandledError(ex);
            }
        }

        [HttpPost]
        public IActionResult Add([FromBody] MedicationAddDTO medicationToAdd)
        {
            try
            {
                var medicationCreated = _medicationService.AddMedication(medicationToAdd);

                var createdItemUri = $"{Request.Path}/{Uri.EscapeDataString(medicationCreated.Id)}";
                return Created(createdItemUri, medicationCreated);
            }
            catch (Exception ex)
            {
                if (ex is NullOrEmptyMedicationNameException
                    || ex is LowerOrEqualToZeroMedicationQuantityException)
                {
                    return BadRequest(ex.Message);
                }

                return HandleGenericunHandledError(ex);
            }
        }

        [HttpDelete("{id}")]
        public IActionResult Delete(string id)
        {
            try
            {
                _medicationService.DeleteMedication(id);

                return NoContent();
            }
            catch (Exception ex)
            {
                if (ex is NullOrEmptyMedicationIdException)
                {
                    return BadRequest(ex.Message);
                }

                return HandleGenericunHandledError(ex);
            }
        }

        private ObjectResult HandleGenericunHandledError(Exception ex)
        {
            _logger.LogError($"An error occurred on '{RouteData.Values["controller"]}\\{RouteData.Values["action"]}':\n {ex.Message} \n StackTrace:{ex.StackTrace}");

            return StatusCode(StatusCodes.Status500InternalServerError, "An error occurred, MedMan Api was unable to satisfy the request.");
        }
    }
}
