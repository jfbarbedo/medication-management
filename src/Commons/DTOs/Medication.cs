﻿using System;

namespace Commons.DTOs
{
    public class Medication
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public int Quantity { get; set; }
        public DateTime CreationDate { get; set; }
    }
}
