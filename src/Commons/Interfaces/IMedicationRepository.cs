﻿using Commons.DTOs;
using System.Collections.Generic;

namespace Commons.Interfaces
{
    public interface IMedicationRepository
    {
        string Add(MedicationAddDTO medicationToAdd);

        void Delete(string id);

        IEnumerable<Medication> GetAll();
    }
}
