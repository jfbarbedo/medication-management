﻿using Commons.DTOs;
using Commons.Interfaces;
using System.Collections.Generic;

namespace Services
{
    public class MedicationService : IMedicationService
    {
        private readonly IMedicationRepository _repository;
        private readonly IMedicationValidator _validator;
        private readonly IMedicationFactory _factory;

        public MedicationService(IMedicationRepository repository, IMedicationValidator validator, IMedicationFactory factory)
        {
            _repository = repository;
            _validator = validator;
            _factory = factory;
        }

        public Medication AddMedication(MedicationAddDTO medicationToAdd)
        {
            _validator.ValidateMedicationToAdd(medicationToAdd);

            var medicationIdcreated = _repository.Add(medicationToAdd);

            var medicationCreated = _factory.CreateMedication(medicationIdcreated, medicationToAdd);

            return medicationCreated;
        }

        public IEnumerable<Medication> GetAllMedication()
        {
            return _repository.GetAll();
        }

        public void DeleteMedication(string id)
        {
            _validator.ValidateMedicationId(id);

            _repository.Delete(id);
        }
    }
}
