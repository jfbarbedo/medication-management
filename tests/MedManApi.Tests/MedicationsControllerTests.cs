using Commons.DTOs;
using Commons.Exceptions;
using Commons.Interfaces;
using MedManApi.Controllers;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Routing;
using Microsoft.Extensions.Logging;
using Moq;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;

namespace MedManApi.Tests
{
    [TestFixture()]
    public class MedicationsControllerTests
    {
        [Test]
        public void GetMedications_NormalRequest_ResturnsMedicationList()
        {
            var medications = new List<Medication>
            {
                new Medication
                {
                    Id = "f96f8380-aca7-4137-a3a3-e419c394f6c7",
                    Name = "Vicodin",
                    Quantity = 9514,
                    CreationDate = DateTime.Parse("1978-01-01T00:00:00")
                },
                new Medication
                {
                    Id = "3dec7dc0-3650-4b07-864f-3dfbef71b83f",
                    Name = "Albuterol",
                    Quantity = 2589,
                    CreationDate = DateTime.Parse("1972-10-21T00:00:00")
                }
            };

            var service = new Mock<IMedicationService>();
            service.Setup(m => m.GetAllMedication())
                .Returns(medications);

            var logger = new Mock<ILogger<MedicationsController>>();

            var controller = new MedicationsController(logger.Object, service.Object);

            var result = controller.GetMedications();

            Assert.NotNull(result);
            Assert.IsInstanceOf<IActionResult>(result);

            var okObjectResult = result as OkObjectResult;
            Assert.NotNull(okObjectResult);
            Assert.AreEqual(200, okObjectResult.StatusCode);
            Assert.NotNull(okObjectResult.Value);
            Assert.AreEqual(medications.Count, ((IEnumerable<Medication>)okObjectResult.Value).Count());
        }

        [Test]
        public void GetMedications_UnexpectedError_ResturnsInternalServerError()
        {
            var service = new Mock<IMedicationService>();
            service.Setup(m => m.GetAllMedication())
                .Throws(new Exception("exception"));

            var logger = new Mock<ILogger<MedicationsController>>();

            var routeData = new RouteData();
            routeData.Values.Add("controller", "controller");
            routeData.Values.Add("action", "action");

            var controller = new MedicationsController(logger.Object, service.Object)
            {
                ControllerContext = new ControllerContext { RouteData = routeData }
            };

            var result = controller.GetMedications();

            Assert.NotNull(result);
            Assert.IsInstanceOf<IActionResult>(result);
            var objResult = result as ObjectResult;
            Assert.NotNull(objResult);
            Assert.AreEqual(500, objResult.StatusCode);
            Assert.AreEqual(false, string.IsNullOrWhiteSpace((string)objResult.Value));
        }

        [Test]
        public void Add_NormalRequest_ReturnsCreated()
        {
            var medicationToAdd = new MedicationAddDTO
            {
                Name = "Vicodin",
                Quantity = 9514,
                CreationDate = DateTime.Parse("1978-01-01T00:00:00")
            };

            var medication = new Medication
            {
                Id = "f96f8380-aca7-4137-a3a3-e419c394f6c7",
                Name = "Vicodin",
                Quantity = 9514,
                CreationDate = DateTime.Parse("1978-01-01T00:00:00")
            };

            var service = new Mock<IMedicationService>();
            service.Setup(m => m.AddMedication(It.IsAny<MedicationAddDTO>()))
                .Returns(medication);

            var logger = new Mock<ILogger<MedicationsController>>();

            var controller = new MedicationsController(logger.Object, service.Object)
            {
                ControllerContext = new ControllerContext
                {
                    HttpContext = new DefaultHttpContext()
                }
            };

            controller.HttpContext.Request.Path = "/medications";
            controller.HttpContext.Request.Method = "POST";

            var result = controller.Add(medicationToAdd);

            Assert.NotNull(result);
            Assert.IsInstanceOf<IActionResult>(result);

            var createdResult = result as CreatedResult;
            Assert.NotNull(createdResult);
            Assert.AreEqual(201, createdResult.StatusCode);
            Assert.NotNull(createdResult.Value);
            Assert.NotNull(createdResult.Location);
            Assert.AreEqual("f96f8380-aca7-4137-a3a3-e419c394f6c7", ((Medication)createdResult.Value).Id);
            Assert.AreEqual("/medications/f96f8380-aca7-4137-a3a3-e419c394f6c7", createdResult.Location);
        }

        [Test]
        public void Add_UnexpectedError_ResturnsInternalServerError()
        {
            var medicationToAdd = new MedicationAddDTO
            {
                Name = "Vicodin",
                Quantity = 9514,
                CreationDate = DateTime.Parse("1978-01-01T00:00:00")
            };

            var service = new Mock<IMedicationService>();
            service.Setup(m => m.AddMedication(It.IsAny<MedicationAddDTO>()))
                .Throws(new Exception("exception"));

            var logger = new Mock<ILogger<MedicationsController>>();

            var routeData = new RouteData();
            routeData.Values.Add("controller", "controller");
            routeData.Values.Add("action", "action");

            var controller = new MedicationsController(logger.Object, service.Object)
            {
                ControllerContext = new ControllerContext { RouteData = routeData }
            };

            var result = controller.Add(medicationToAdd);

            Assert.NotNull(result);
            Assert.IsInstanceOf<IActionResult>(result);
            var objResult = result as ObjectResult;
            Assert.NotNull(objResult);
            Assert.AreEqual(500, objResult.StatusCode);
            Assert.AreEqual(false, string.IsNullOrWhiteSpace((string)objResult.Value));
        }

        [Test]
        public void Add_QuantityEqualsToZero_ResturnsBadRequest()
        {
            var medicationToAdd = new MedicationAddDTO
            {
                Name = "Vicodin",
                Quantity = 0,
                CreationDate = DateTime.Parse("1978-01-01T00:00:00")
            };

            var exceptionMessage = "exception message";

            var service = new Mock<IMedicationService>();
            service.Setup(m => m.AddMedication(It.IsAny<MedicationAddDTO>()))
                .Throws(new LowerOrEqualToZeroMedicationQuantityException(exceptionMessage));

            var logger = new Mock<ILogger<MedicationsController>>();

            var controller = new MedicationsController(logger.Object, service.Object);

            var result = controller.Add(medicationToAdd);

            Assert.NotNull(result);
            Assert.IsInstanceOf<IActionResult>(result);
            var badRequestResult = result as BadRequestObjectResult;
            Assert.NotNull(badRequestResult);
            Assert.AreEqual(StatusCodes.Status400BadRequest, badRequestResult.StatusCode);
            Assert.NotNull(badRequestResult.Value);
            Assert.AreEqual(exceptionMessage, badRequestResult.Value);
        }

        [Test]
        public void Delete_NormalRequest_ReturnsNoContent()
        {
            var id = "f96f8380-aca7-4137-a3a3-e419c394f6c7";

            var service = new Mock<IMedicationService>();
            service.Setup(m => m.DeleteMedication(It.IsAny<string>()));

            var logger = new Mock<ILogger<MedicationsController>>();

            var controller = new MedicationsController(logger.Object, service.Object);

            var result = controller.Delete(id);

            Assert.NotNull(result);
            Assert.IsInstanceOf<IActionResult>(result);
            var noContentResult = result as NoContentResult;
            Assert.AreEqual(StatusCodes.Status204NoContent, noContentResult.StatusCode);
        }

        [Test]
        public void Delete_EmptyStringId_ReturnsBadRequest()
        {
            string id = "";
            var exceptionMessage = "exception message";

            var service = new Mock<IMedicationService>();
            service.Setup(m => m.DeleteMedication(It.IsAny<string>()))
                .Throws(new NullOrEmptyMedicationIdException(exceptionMessage));

            var logger = new Mock<ILogger<MedicationsController>>();

            var controller = new MedicationsController(logger.Object, service.Object);

            var result = controller.Delete(id);

            Assert.NotNull(result);
            Assert.IsInstanceOf<IActionResult>(result);
            var badRequestResult = result as BadRequestObjectResult;
            Assert.NotNull(badRequestResult);
            Assert.AreEqual(StatusCodes.Status400BadRequest, badRequestResult.StatusCode);
            Assert.NotNull(badRequestResult.Value);
            Assert.AreEqual(exceptionMessage, badRequestResult.Value);
        }

        [Test]
        public void Delete_UnexpectedError_ResturnsInternalServerError()
        {
            var id = "f96f8380-aca7-4137-a3a3-e419c394f6c7";

            var exceptionMessage = "exception message";

            var service = new Mock<IMedicationService>();
            service.Setup(m => m.DeleteMedication(It.IsAny<string>()))
                .Throws(new Exception(exceptionMessage));

            var logger = new Mock<ILogger<MedicationsController>>();

            var routeData = new RouteData();
            routeData.Values.Add("controller", "controller");
            routeData.Values.Add("action", "action");

            var controller = new MedicationsController(logger.Object, service.Object)
            {
                ControllerContext = new ControllerContext { RouteData = routeData }
            };

            var result = controller.Delete(id);

            Assert.NotNull(result);
            Assert.IsInstanceOf<IActionResult>(result);
            var objResult = result as ObjectResult;
            Assert.NotNull(objResult);
            Assert.AreEqual(500, objResult.StatusCode);
            Assert.AreEqual(false, string.IsNullOrWhiteSpace((string)objResult.Value));
        }
    }
}