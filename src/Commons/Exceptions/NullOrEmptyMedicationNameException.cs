﻿using System;

namespace Commons.Exceptions
{
    public class NullOrEmptyMedicationNameException : Exception
    {
        public NullOrEmptyMedicationNameException(string message, Exception innerException) : base(message, innerException)
        {
        }

        public NullOrEmptyMedicationNameException(string message) : base(message)
        {
        }
    }
}
