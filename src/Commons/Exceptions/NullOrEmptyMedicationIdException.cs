﻿using System;

namespace Commons.Exceptions
{
    public class NullOrEmptyMedicationIdException : Exception
    {
        public NullOrEmptyMedicationIdException(string message, Exception innerException) : base(message, innerException)
        {
        }

        public NullOrEmptyMedicationIdException(string message) : base(message)
        {
        }
    }
}
