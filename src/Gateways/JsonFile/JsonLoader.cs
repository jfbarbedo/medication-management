﻿using Commons.Configurations;
using Commons.DTOs;
using Gateways.JsonFile.Interfaces;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;

namespace Gateways.JsonFile
{
    public class JsonLoader : IJsonLoader
    {
        private string _filePath;

        public JsonLoader(IOptions<JsonDatabaseSettings> jsonDatabaseSettings)
        {
            _filePath = jsonDatabaseSettings.Value.FilePath;

            LoadJsonFile();
        }

        public IEnumerable<Medication> LoadJsonFile()
        {
            if (File.Exists(_filePath))
            {
                using (var streamReader = new StreamReader(_filePath))
                {
                    var json = streamReader.ReadToEnd();
                    return JsonConvert.DeserializeObject<List<Medication>>(json);
                }
            }

            throw new Exception("No json file found.");
        }
    }
}
