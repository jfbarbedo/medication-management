﻿using Commons.DTOs;
using Commons.Interfaces;
using Gateways.JsonFile.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Gateways.JsonFile
{
    public class MedicationFromJsonFileRepository : IMedicationRepository
    {
        private List<Medication> _medicationsCache;

        private readonly IMedicationFactory _factory;
        private readonly IJsonLoader _jsonLoader;
        private readonly IJsonPersister _jsonPersister;

        public MedicationFromJsonFileRepository(IMedicationFactory factory, IJsonLoader jsonLoader, IJsonPersister jsonPersister)
        {
            _factory = factory;
            _jsonLoader = jsonLoader;
            _jsonPersister = jsonPersister;

            _medicationsCache = _jsonLoader.LoadJsonFile().ToList();
        }

        public string Add(MedicationAddDTO medicationToAdd)
        {
            var medication = _factory.CreateMedication(medicationToAdd);

            _medicationsCache.Add(medication);

            _jsonPersister.Persist(_medicationsCache);

            return medication.Id;
        }

        public void Delete(string id)
        {
            _medicationsCache.RemoveAll(m => string.Equals(m.Id, id, StringComparison.InvariantCultureIgnoreCase));

            _jsonPersister.Persist(_medicationsCache);
        }

        public IEnumerable<Medication> GetAll()
        {
            return _medicationsCache.ToList();
        }
    }
}
