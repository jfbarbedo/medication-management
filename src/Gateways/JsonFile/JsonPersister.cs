﻿using Commons.Configurations;
using Commons.DTOs;
using Gateways.JsonFile.Interfaces;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.IO;

namespace Gateways.JsonFile
{
    public class JsonPersister : IJsonPersister
    {
        private string _filePath;

        public JsonPersister(IOptions<JsonDatabaseSettings> jsonDatabaseSettings)
        {
            _filePath = jsonDatabaseSettings.Value.FilePath;
        }

        public void Persist(IEnumerable<Medication> medications)
        {
            using (var streamWriter = new StreamWriter(_filePath))
            {
                var json = JsonConvert.SerializeObject(medications);
                streamWriter.Write(json);
            }
        }
    }
}
