﻿using Commons.DTOs;
using Commons.Exceptions;
using Commons.Interfaces;

namespace Validators
{
    public class MedicationValidator : IMedicationValidator
    {
        public void ValidateMedicationId(string id)
        {
            if (string.IsNullOrWhiteSpace(id))
            {
                throw new NullOrEmptyMedicationIdException("Medication Id should not be empty.");
            }
        }

        public void ValidateMedicationToAdd(MedicationAddDTO medicationToAdd)
        {
            if (string.IsNullOrWhiteSpace(medicationToAdd.Name))
            {
                throw new NullOrEmptyMedicationNameException("Name should not be null or empty.");
            }

            if (medicationToAdd.Quantity <= 0)
            {
                throw new LowerOrEqualToZeroMedicationQuantityException("Medication quantity should be greater than zero.");
            }
        }
    }
}
