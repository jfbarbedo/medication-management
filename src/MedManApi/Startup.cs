using Commons.Configurations;
using Commons.Interfaces;
using Factories;
using Gateways.JsonFile;
using Gateways.JsonFile.Interfaces;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.OpenApi.Models;
using Services;
using Validators;

namespace MedManApi
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        public void ConfigureServices(IServiceCollection services)
        {
            services.Configure<JsonDatabaseSettings>(Configuration.GetSection("JsonDatabaseSettings"));

            services.AddTransient<IMedicationService, MedicationService>();
            services.AddTransient<IMedicationValidator, MedicationValidator>();
            services.AddTransient<IMedicationFactory, MedicationFactory>();

            services.AddSingleton<IMedicationRepository, MedicationFromJsonFileRepository>();
            services.AddSingleton<IJsonLoader, JsonLoader>();
            services.AddSingleton<IJsonPersister, JsonPersister>();

            services.AddControllers();
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new OpenApiInfo { Title = "MedManApi", Version = "v1" });
            });
        }

        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseSwagger();
                app.UseSwaggerUI(c => c.SwaggerEndpoint("/swagger/v1/swagger.json", "MedManApi v1"));
            }

            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
