﻿using Commons.DTOs;
using System.Collections.Generic;

namespace Gateways.JsonFile.Interfaces
{
    public interface IJsonPersister
    {
        void Persist(IEnumerable<Medication> medications);
    }
}
