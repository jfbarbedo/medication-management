﻿using Commons.DTOs;
using Commons.Interfaces;
using System;

namespace Factories
{
    public class MedicationFactory : IMedicationFactory
    {
        public Medication CreateMedication(string id, MedicationAddDTO medicationToAdd)
        {
            return new Medication
            {
                Id = id,
                Name = medicationToAdd.Name,
                Quantity = medicationToAdd.Quantity,
                CreationDate = medicationToAdd.CreationDate
            };
        }

        public Medication CreateMedication(MedicationAddDTO medicationToAdd)
        {
            return new Medication
            {
                Id = Guid.NewGuid().ToString(),
                Name = medicationToAdd.Name,
                Quantity = medicationToAdd.Quantity,
                CreationDate = medicationToAdd.CreationDate
            };
        }
    }
}
